package show_options;

import java.io.IOException;

import StageMaster.StageMaster;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;



public class ShowOption{
	
	public void show() {
		try{
			Parent actorGroup =FXMLLoader.load(getClass().getResource("option pm.fxml"));
			StageMaster.getStage().setScene(new Scene(actorGroup));
			StageMaster.getStage().show();
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
}
