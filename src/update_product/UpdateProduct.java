package update_product;

import java.io.IOException;

import StageMaster.StageMaster;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class UpdateProduct{
	public void show() {
		try {
			Parent actorGroup = FXMLLoader.load(getClass().getResource(getClass().getSimpleName()+".fxml"));
			StageMaster.getStage().setScene(new Scene(actorGroup));
			StageMaster.getStage().show();
			
		}
		catch(IOException e) {
			e.printStackTrace();
		}
	}

}
