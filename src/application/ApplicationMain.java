package application;

import java.awt.SplashScreen;

import StageMaster.StageMaster;
import db_operation.DbUtil;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import show_options.ShowOption;

public class ApplicationMain extends Application{
	public static void main(String args[]) {
		DbUtil.createDbConnection();
		launch(args);
		
	}
	public void start(Stage primaryStage) {
		StageMaster.setStage(primaryStage);
		new ShowOption().show();
	}
	

}

