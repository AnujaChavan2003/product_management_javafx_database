package search_product;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class SearchProduct{
	
	public void start(Stage stageProvidedByJfx) throws Exception {
		Parent actorGroup =FXMLLoader.load(getClass().getResource(getClass().getSimpleName()+".fxml"));
		
		stageProvidedByJfx.setTitle("SearchProduct");
		Scene scene = new Scene(actorGroup,600,400);
		stageProvidedByJfx.setScene(scene);
		stageProvidedByJfx.show();
		
	}
}
